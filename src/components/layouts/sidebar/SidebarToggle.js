import './SidebarToggle.scss'

export default {
  bind () {
    let clicked = () => this.vm.$dispatch('toggle-sidebar')
    this.el.addEventListener('click', clicked)
    this.el.className += ' app-sidebar-toggle'
    this.vm.$on('toggle-sidebar', (isActive) => {
      if (isActive === true) {
        this.el.className += ' sidebar-active'
      } else if (isActive === false) {
        let curClass = this.el.className
        this.el.className = curClass.replace(' sidebar-active', '')
      }
    })
    this.unbind = () => this.el.removeEventListener('click', clicked)
  }
}
