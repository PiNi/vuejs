import './Tabs.scss'

const Tabs = {
  bind () {
    this.vm.$on('tab-show', (tabId) => {
      this.vm.$broadcast('tab-show', tabId)
    })
  }
}

const TabsLink = {
  bind () {
    let clicked = () => {
      this.el.className += ' tab-link'
      this.vm.$dispatch('tab-show', this.expression)
    }
    this.vm.$on('tab-show', (tabId) => {
      this.activate(this.expression === tabId)
    })
    this.el.addEventListener('click', clicked)
  },
  activate (active) {
    if (active) {
      this.el.className += ' active'
    } else {
      let className = this.el.className
      this.el.className = className.replace(' active', '')
    }
  }
}

const TabsContent = {
  bind () {
    this.el.className += ' tab-content'
    this.vm.$on('tab-show', (tabId) => {
      this.activate(this.expression === tabId)
    })
  },
  activate (active) {
    if (active) {
      this.el.className += ' active'
    } else {
      let className = this.el.className
      this.el.className = className.replace(' active', '')
    }
  }
}

export {Tabs, TabsLink, TabsContent}
