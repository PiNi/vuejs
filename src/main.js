import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import VueValidator from 'vue-validator'

// Css imports
import './assets/font-awesome/css/font-awesome.css'

// Http config
Vue.use(VueResource)
Vue.http.options.root = 'http://jsonplaceholder.typicode.com'

// Validator plugin
Vue.use(VueValidator)

// Router
import App from './App'
import Components from './app/Components'
import Posts from './app/Posts'

Vue.use(VueRouter)

let router = new VueRouter()
router.map({
  '/posts': {
    name: 'posts',
    component: Posts
  },
  '/components': {
    name: 'components',
    component: Components
  }
})
router.start(App, 'body')

